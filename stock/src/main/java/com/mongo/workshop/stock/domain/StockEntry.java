
package com.mongo.workshop.stock.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

//This is a mapping with Data Model, it is more simple and intuitive
@Getter
@Setter
public final class StockEntry {
    private String id;
    private Date createdDatetime;
    private Date updatedDatetime;
    private String myValue;
}