// ******************************************************
//  Copyright (C) 2019 Li Cheuk Wai, Tony <tony.li@mongodb.com>
//
//  This file is a part of Hong Kong Hospital Authority Consulting.
//
//  Hong Kong Hospital Authority Consulting can not be copied and/or distributed without the express
//  permission of Li Cheuk Wai, Tony
//  *******************************************************
//
// this file aims to provide the logic or mongoDB Repository

package com.mongo.workshop.stock.service;

import com.mongo.workshop.stock.domain.StockEntry;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import static com.mongodb.client.model.Filters.eq;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Repository
public class MongoDBService{
    private final Logger logger= LoggerFactory.getLogger(MongoDBService.class);

    private MongoDatabase dbTarget;
    private MongoCollection<StockEntry> colStockEntry;

    /***
     * This constructor and it establishes connections with parameters in the URI
     * It also create the POJO Codec for POJO Class and Data Model Mapping
     *
     * @param theMongoDBURL
     * @param theTargetDB
     * @param theTargetCol
     */
    @Autowired
    public MongoDBService(@Value("${mongodbURL}") String theMongoDBURL, @Value("${targetDB}") String theTargetDB, @Value("${targetCol}") String theTargetCol){
        logger.info("MongoDBService Started");

        logger.info("Connecting to " + theMongoDBURL);

        //Establish Connection to mongoDB through URI
        //URI expects
        //SSL = true?
        //Default connection pool = 100 in this process
        //Default waitQueueMultiple = 5.0
        //Default waitQUeueTimeoutMS = 120000
        MongoClient mongoClient = MongoClients.create(theMongoDBURL);

        // create codec registry for POJOs
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        dbTarget = mongoClient.getDatabase(theTargetDB).withCodecRegistry(pojoCodecRegistry);
        colStockEntry = dbTarget.getCollection(theTargetCol, StockEntry.class);

    }

    public int insertANewDoc(StockEntry theDoc){
        logger.info(String.format("Try to Write"));


        colStockEntry.insertOne(theDoc);

        //Successsful update
        return 1;
    }


    public StockEntry readADoc(String theID){
        logger.info(String.format("Try to Read with ID: %s", theID));
        StockEntry _stockEntry = colStockEntry.find(eq("_id", theID)).first();
        logger.info(String.format("Try to Read with ID %s and myValue is : %s", theID, _stockEntry.getMyValue()));
        return _stockEntry;

    }

}
