package com.mongo.workshop.stock.controller;

import com.mongo.workshop.stock.domain.StockEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.mongo.workshop.stock.service.MongoDBService;


import static java.util.Objects.isNull;

@RestController
@RequestMapping("/api")
public class HelloWorldController {

    private final Logger logger= LoggerFactory.getLogger(HelloWorldController.class);

    //This is the main mongoDB Service
    @Autowired
    private MongoDBService mongoDBService;

    @PostMapping(value = "/hello")
    public String hello () {
        return "hello";
    }

    @GetMapping("/readADoc/{id}")
    public String readADoc(@PathVariable String id){
        StockEntry _Result = mongoDBService.readADoc(id);
        return isNull(_Result)?   "No Doc Found" : _Result.toString() ;
    }

    @GetMapping("/insertADoc/{Value}")
    public String upsertADoc(@PathVariable String Value){
        int _Result = 0;

        StockEntry _stockEntry = new StockEntry();
        _stockEntry.setMyValue(Value);


        if (mongoDBService.insertANewDoc(_stockEntry)!= 0){
                _Result = 1;
        }

        if(_Result ==0){
            throw new RuntimeException("Doc cannot be inserted");
        }

        return _Result==1 ? "Doc Insert": "Doc Failed to Insert";
    }


}
